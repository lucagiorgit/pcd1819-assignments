package pcd1819.ex1btasks;

import pcd1819.ex1tasks.ForkJoinFileReader;
import pcd1819.ex1tasks.ForkJoinUtils;
import pcd1819.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class ForkJoinContinuousFilesReader extends RecursiveTask<Void> {
    private List<File> files;
    private final BlockingQueue<Map<String, Long>> blockingQueue;

    public ForkJoinContinuousFilesReader(List<File> files, BlockingQueue<Map<String,Long>> blockingQueue){
        this.files = files;
        this.blockingQueue = blockingQueue;
    }

    @Override
    protected Void compute() {
        System.out.println("ForkJoinContinuousFilesReader: for " + files.size() + " files.");
        List<RecursiveTask<Map<String,Long>>> tasks = ForkJoinUtils.tasksForItems(files, (file,k) -> new ForkJoinContinuousFileReader(file, blockingQueue));
        tasks.stream().map(task -> task.join());
        return null;
    }
}
