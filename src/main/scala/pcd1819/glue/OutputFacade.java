package pcd1819.glue;

import javafx.beans.property.Property;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import pcd1819.model.ProcessingStatus;

public class OutputFacade {
    public final ObservableList<String> outputList;
    public final Property<String> latency;

    public OutputFacade(ObservableList<String> outputList,
                        Property<String> latency){
        this.outputList = outputList;
        this.latency = latency;
    }
}
