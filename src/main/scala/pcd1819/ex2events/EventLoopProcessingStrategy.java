package pcd1819.ex2events;

import io.vertx.core.Vertx;
import pcd1819.ex1tasks.ForkJoinFilesReader;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStrategy;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class EventLoopProcessingStrategy extends ProcessingStrategy {
    private Vertx vertx;
    private WordCounterVerticle verticle;

    public EventLoopProcessingStrategy(InputFacade input, OutputFacade output) {
        super(input, output);
        this.vertx = Vertx.vertx();
        vertx.eventBus().registerDefaultCodec(ConcurrentHashMap.class, new ConcurrentHashMapCodec());
    }

    @Override
    public void start() {
        super.start();
        if(!this.active) return;

        this.verticle = new WordCounterVerticle(input, output);
        this.vertx.deployVerticle(this.verticle);
    }

    @Override
    public void stop() {
        super.stop();

        this.vertx.undeploy(verticle.deploymentID());
    }
}
