package pcd1819.ex2events;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

import java.util.concurrent.ConcurrentHashMap;

class ConcurrentHashMapCodec implements MessageCodec<ConcurrentHashMap,ConcurrentHashMap> {
    @Override
    public void encodeToWire(Buffer buffer, ConcurrentHashMap concurrentHashMap) {
    }

    @Override
    public ConcurrentHashMap decodeFromWire(int pos, Buffer buffer) {
        return new ConcurrentHashMap();
    }

    @Override
    public ConcurrentHashMap transform(ConcurrentHashMap concurrentHashMap) {
        return concurrentHashMap;
    }

    @Override
    public String name() {
        return "ConcurrentHashMapCodec";
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
