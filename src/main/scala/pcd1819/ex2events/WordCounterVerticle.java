package pcd1819.ex2events;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.FileSystem;
import io.vertx.core.file.OpenOptions;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.utils.Utils;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordCounterVerticle extends AbstractVerticle {
    public static final String PARTIAL_RESULT_AVAILABLE = "start";
    public static final String UPDATE_GUI = "update";

    private final InputFacade input;
    private final OutputFacade output;
    private Map<String,Long> result;
    private long startTime;
    private int totFiles;
    private int totFilesProcessed;

    public WordCounterVerticle(InputFacade input, OutputFacade output){
        this.input = input;
        this.output = output;
    }

    @Override
    public void start() throws Exception {
        super.start();

        result = new HashMap<>();

        startTime = System.nanoTime();

        input.inputSet.forEach(filePath -> processFile(filePath));
        totFiles = input.inputSet.size();
        totFilesProcessed = 0;

        input.inputSet.addListener((ListChangeListener<? super String>) change -> {
            vertx.executeBlocking(fut -> {
            while(change.next()) {
                if(change.wasAdded()) {
                    totFiles += change.getAddedSize();
                    change.getAddedSubList().forEach(filePath -> processFile(filePath));
                }
            }
            fut.complete();
            }, done -> { startTime = System.nanoTime(); });
        });

        vertx.eventBus().consumer(PARTIAL_RESULT_AVAILABLE, msg -> {
            Map<String,Long> partialRes = (Map<String, Long>) msg.body();
            Utils.mergeMapsMutable(result, partialRes);

            totFilesProcessed++;
            if(totFilesProcessed==totFiles || totFilesProcessed % Utils.FREQ_UPDATE_EACH_K_FILES == 0)
                vertx.eventBus().send(UPDATE_GUI, "");
        });

        vertx.eventBus().consumer(UPDATE_GUI, msg -> {
            List<String> finalOutput = Utils.moreFrequentWords(result, Utils.K);
            Platform.runLater(()-> {
                output.outputList.setAll(finalOutput);
                output.latency.setValue(Duration.ofNanos(System.nanoTime()-startTime).toMillis()+"ms");
            });
        });
    }

    public void processFile(String filePath){
        FileSystem fs = vertx.fileSystem();

        // NOTE: fs.readFile() reads the file fully (not chunk-by-chunk)
        // So, for big files, other approaches are preferred
        // https://stackoverflow.com/questions/45677070/read-large-file-using-vertx

        fs.readFile(filePath, buff -> {
               vertx.executeBlocking(future -> {
                   String txt = buff.toString();
                   future.complete(Utils.countWordsInTxt(txt, Utils.N));
               }, countingAsyncRes -> {
                   Map<String, Long> partialRes = (Map<String, Long>) countingAsyncRes.result();
                   vertx.eventBus().send(PARTIAL_RESULT_AVAILABLE, partialRes);
               });
        });
    }

    public void processFileV2(String filePath){
        FileSystem fs = vertx.fileSystem();

        // NOTE: fs.readFile() reads the file fully (not chunk-by-chunk)
        // So, for big files, other approaches are preferred
        // https://stackoverflow.com/questions/45677070/read-large-file-using-vertx

        fs.open(filePath, new OpenOptions().setWrite(false).setCreate(false), readFileAsyncRes -> {
           AsyncFile af = readFileAsyncRes.result();
           af.setReadBufferSize(Utils.SPLIT_CHUNK_SIZE);
           af.handler(buff -> {
            vertx.executeBlocking(future -> {
                String txt = buff.toString();
                future.complete(Utils.countWordsInTxt(txt, Utils.N));
            }, countingAsyncRes -> {
                Map<String, Long> partialRes = (Map<String, Long>) countingAsyncRes.result();
                vertx.eventBus().send(PARTIAL_RESULT_AVAILABLE, partialRes);
            });
        })
        /*.endHandler((h) -> {
               // We choose to update results once a file is
               vertx.eventBus().send(UPDATE_GUI, "");
           })*/; // DON'T KNOW WHY, BUT endHandler SEEMS NOT BE CALLED
        });
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("Asked to stop");
    }

}
