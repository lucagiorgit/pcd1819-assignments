package pcd1819.ex1tasks;

import pcd1819.utils.Utils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class ForkJoinFilesReader extends RecursiveTask<Map<String,Long>> {
    private List<File> files;
    private final Consumer<Map<String, Long>> resultHandler;
    private Map<String,Long> result;

    public ForkJoinFilesReader(List<File> files, Consumer<Map<String,Long>> resultHandler){
        this.files = files;
        this.resultHandler = resultHandler;
    }

    /*
     * ISSUE: how to deal with a dynamic input set (which can change over time) ???
     */
    @Override
    protected Map<String,Long> compute() {

        List<RecursiveTask<Map<String,Long>>> tasks = ForkJoinUtils.tasksForItems(files, (file,k) -> new ForkJoinFileReader(file));

        Map<String,Long> result = new HashMap<>();

        AtomicInteger k = new AtomicInteger();
        tasks.stream().map(task -> task.join())
                .forEach(map -> {
                    Utils.mergeMapsMutable(result, map);
                    k.getAndIncrement();
                    if(k.get()==files.size() || k.get() % Utils.FREQ_UPDATE_EACH_K_FILES==0)
                        resultHandler.accept(result);
                });
        this.result = result;
        return result;
    }
}
