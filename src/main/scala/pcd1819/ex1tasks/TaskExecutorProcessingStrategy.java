package pcd1819.ex1tasks;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import pcd1819.glue.InputFacade;
import pcd1819.glue.OutputFacade;
import pcd1819.model.ProcessingStrategy;
import pcd1819.utils.Utils;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TaskExecutorProcessingStrategy extends ProcessingStrategy {

    private ForkJoinPool fjpool;
    private ForkJoinTask<Map<String,Long>> task;
    private ListChangeListener<? super String> fileListListener;

    public TaskExecutorProcessingStrategy(InputFacade input, OutputFacade output) {
        super(input, output);
    }

    @Override
    public void start() {
        super.start();
        if(!this.active) return;

        long startTime = System.nanoTime();

        Consumer<Map<String,Long>> resultHandler = (Map<String,Long> result) -> {
            // log.debug("Updating results: " + result);
            List<String> resList = Utils.moreFrequentWords(result, Utils.K);
            Platform.runLater( () -> {
                output.latency.setValue(Duration.ofNanos(System.nanoTime()-startTime).toMillis()+"ms");
                output.outputList.clear();
                output.outputList.addAll(resList);
            });
        };

        fjpool = new ForkJoinPool();
        List<File> files = input.inputSet.stream().map(s -> new File(s)).collect(Collectors.toList());

        fileListListener = change -> {
            while (change.next()) {
                if (change.wasAdded()) {
                    List<File> newFiles = change.getAddedSubList().stream().map(s -> new File(s)).collect(Collectors.toList());

                    fjpool.submit(new ForkJoinFilesReader(newFiles, resultHandler));
                }
            }
        };
        input.inputSet.addListener(fileListListener);

        task = fjpool.submit(new ForkJoinFilesReader(files, resultHandler));
    }

    @Override
    public void stop() {
        super.stop();
        fjpool.shutdownNow();
        if(fileListListener!=null) input.inputSet.removeListener(fileListListener);
    }
}
