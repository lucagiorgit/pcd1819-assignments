import org.gradle.internal.os.OperatingSystem


val scalaMajor = "2.12"
val scalaVersion = "${scalaMajor}.8"

plugins {
    java
    scala
    application
    id("org.openjfx.javafxplugin") version "0.0.7"
}

application {
    mainClassName = "pcd1819.MainApp"
}

repositories {
    jcenter()
}

buildscript {
    repositories {
        maven {
            setUrl("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("org.openjfx:javafx-plugin:0.0.7")
    }
}

val os = OperatingSystem.current()!!
val platform = when { os.isWindows -> "win"; os.isLinux-> "linux"; os.isMacOsX -> "mac"; else -> error("Unknown OS") }

dependencies {
    implementation("org.scala-lang:scala-library:2.12.8")
    implementation("com.google.guava:guava:23.0")

    implementation("io.vertx:vertx-core:3.7.0")

    implementation("io.reactivex.rxjava2:rxjava:2.1.3")

    implementation("org.slf4j:slf4j-log4j12:1.7.26")
    implementation("log4j:log4j:1.2.17")
}

javafx {
    modules("javafx.controls", "javafx.fxml")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<ScalaCompile>().configureEach {
    options.apply {
        targetCompatibility = JavaVersion.VERSION_1_8.toString()
        sourceCompatibility = JavaVersion.VERSION_1_8.toString()
    }
}